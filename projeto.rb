require "./bd.rb"
require './departamento.rb'
class Projeto
    attr_accessor :nome,:cliente ,:fundos ,:departamento

    def initialize params
        @nome = params[:nome]
        @cliente = []
        @fundos = params[:fundos]
        @departamento = params[:departamento]
    end

    def add_departamento departamento
        if @fundos > Bd.soma_todos_salarios departamento
            @departamento.append(departamento)
            Bd.add_depart departamento
        end
    end
end