class Dependente
    attr_accessor :nome, :idade, :parentesco

    def initialize params
        @nome = params[:nome]
        @idade = params[:idade]
        @parentesco = params[:parentesco]
    end
end