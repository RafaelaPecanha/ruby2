require './bd.rb'
require './funcionario.rb'
require './departamento.rb'
require './projeto.rb'
require './dependente.rb'
require 'faker'

funcionario = Funcionario.new({
        s_nome: Faker::Name.first_name,
        p_nome: Faker::Name.last_name,
        idade: Faker::Number.between(from: 10, to:60),
        salario: Faker::Number.decimal(l_digits: 4, r_digits:2),
        endereco: Faker::Address.city,
        dependentes: Faker::Name.name, 
        setor: Faker::Appliance.brand,
        matricula: Faker::Number.number(digits:3),
})

dependente = Dependente.new({
    nome:Faker::Name.name, 
    idade: Faker::Number.between(from: 10, to:60),
    parentesco: Faker::Appliance.brand,
})


projeto = Projeto.new({
    nome:Faker::Name.name, 
    cliente:Faker::Name.name, 
    fundos:Faker::Number.decimal(l_digits: 4, r_digits:2),
    departamento:Faker::Name.name, 
})


departamento = Departamento.new({
    funcionario:Faker::Name.name,
    nome:Faker::Name.name,
    gerente:Faker::Appliance.brand,
})


puts funcionario.nome_completo

funcionario.add_dependentes dependente
print funcionario.dependentes 

puts funcionario.calcular_dependentes 

departamento.add_funcionario funcionario
print departamento.funcionario

departamento.add_gerente funcionario
puts departamento.funcionario

departamento.calcula_salario funcionario
puts departamento.funcionario

departamento.calcula_funcionario departamento
puts departamento.departamento

projeto.add_departamento departamento
puts projeto.departamento