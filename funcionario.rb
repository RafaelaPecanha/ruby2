require "./bd.rb"
class Funcionario
    attr_accessor :p_nome, :s_nome, :idade, :salario, :endereco, :dependentes, :endereco, :setor, :matricula

    def initialize params
        @p_nome = params[:p_nome]
        @s_nome = params[:s_nome]
        @idade = params[:idade]
        @salario = params[:salario]
        @endereco = params[:endereco]
        @dependentes = []
        @setor = params[:setor]
        @matricula = params[:matricula]
    end

    def nome_completo 
        "#{@p_nome} #{@s_nome}"
    end

    def add_dependentes dependente
        @dependentes.append(dependente)
        Bd.add_depen dependente
    end
    
    def calcular_dependentes 
        @dependentes.count
    end 
end