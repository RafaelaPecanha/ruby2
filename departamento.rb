require "./bd.rb"
class Departamento
    attr_accessor :funcionario, :nome, :gerente

    def initialize params
        @funcionarios = []
        @nome = params[:nome]
        @gerente = []
    end

    def add_funcionario funcionario
        @funcionarios.append(funcionario)
        Bd.add_fun funcionario
    end

    def calcula_salario funcionario
       Bd.soma_todos_salarios funcionario
    end

    def calcula_funcionario departamento
        Bd.soma_todos_funcionarios departamento
    end

    def add_gerente funcionario
          @gerente = funcionario
    end
end